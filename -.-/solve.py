from Crypto.Util.number import *
arr = []
arr.append('di-dah') #A
arr.append('dah-di-di-dit') #B
arr.append('dah-di-dah-dit') #C
arr.append('dah-di-dit') #D
arr.append('dit') #E
arr.append('di-di-dah-dit') #F
arr.append('dah-dah-dit') #G
arr.append('di-di-di-dit') #H
arr.append('di-dit') #I
arr.append('di-dah-dah-dah') #J
arr.append('dah-di-dah') #K
arr.append('di-dah-di-dit') #L
arr.append('dah-dah') #M
arr.append('dah-dit') #N
arr.append('dah-dah-dah') #O
arr.append('di-dah-dah-dit') #P
arr.append('dah-dah-di-dah') #Q
arr.append('di-dah-dit') #R
arr.append('di-di-dit') #S
arr.append('dah') #T
arr.append('di-di-dah') #U
arr.append('di-di-di-dah') #V
arr.append('di-dah-dah') #W
arr.append('dah-di-di-dah') #X
arr.append('dah-di-dah-dah') #Y
arr.append('dah-dah-di-dit') #Z
arr.append('dah-dah-dah-dah-dah') #0
arr.append('di-dah-dah-dah-dah') #1
arr.append('di-di-dah-dah-dah') #2
arr.append('di-di-di-dah-dah') #3
arr.append('di-di-di-di-dah') #4
arr.append('di-di-di-di-dit') #5
arr.append('dah-di-di-di-dit') #6
arr.append('dah-dah-di-di-dit') #7
arr.append('dah-dah-dah-di-dit') #8
arr.append('dah-dah-dah-dah-dit') #9

trans = []
k = 'A'
for i in range(0,26):
    g = chr(ord(k)+i)
    trans.append(g)
k = '0'
for i in range(0,10):
    g = chr(ord(k)+i)
    trans.append(g)
data = open('flag.txt','r').read()
split_data = data.split(' ')
ptext = ''
for i in range(len(split_data)):
    for j in range(len(arr)):
        if split_data[i] == arr[j]:
            ptext += trans[j]
print long_to_bytes(int(ptext,16))
