from gmpy2 import *
from Crypto.Util.number import *

def check(sequence): #This function to check a array is superincreasing array?
    total = 0
    for n in sequence:
        if n <= total: 
            return False
        total += n
    return True

def create_prime_array(primes_arr): #This function to create a primes array, which is possible cases of n

    for i in range(3,20000):
        if isPrime(i):
            primes_arr.append(i)

def max_of_arr(arr):
    max = arr[0]
    for i in range(1,len(arr)):
        if arr[i]>max:
            max = arr[i]
    return max

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    g, y, x = egcd(b%a,a)
    return (g, x - (b//a) * y, y)

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('No modular inverse')
    return x%m

def bruteforce(pub_key,primes_arr):
    m = max_of_arr(pub_key) + 1 # modulus(m) > max_value in pub_key
    while m<20000:
        i = 0
        while m > primes_arr[i]:
            if (m % primes_arr[i] != 0): # Because gcd(m,n) = 1
                inv_num = modinv(primes_arr[i],m)
                pri_key = []
                for k in range(len(pub_key)):
                    pri_key.append(pub_key[k] * inv_num % m)
                if check(pri_key):
                    return pri_key, inv_num, m
                i+=1
            else:
                break
        m+=1
pubkey = [99, 1235, 865, 990, 5, 1443, 895, 1477]
primes = []
create_prime_array(primes)
print bruteforce(pubkey,primes)


