Sau khi tìm kiếm trên Google với từ khóa Knapsack cryptosystem, ta biết được thông tin của loại mã hóa sử dụng trong challenge là Merkle–Hellman_knapsack_cryptosystem. Link: https://en.wikipedia.org/wiki/Merkle-Hellman_knapsack_cryptosystem.

Do các số trong pub_key khá là nhỏ, nên mình thử brute-force luôn. Thuật toán Brute force của mình như sau:

1. Khởi tạo mảng các số nguyên tố nhỏ hơn 20000, đây là các trường hợp mình đoán là số n trong private_key (do mình nghĩ là n,m không quá 20000, nếu không tìm thấy thì mình sẽ tăng giá trị này lên).

2. Cho m = max_value(pub_key) + 1, dĩ nhiên modulus phải lớn hơn các giá trị trong pub_key rồi.

3. Chạy 2 vòng lặp của m tăng dần đến 20000 và n tăng dần đến m. Ứng với các giá trị m,n cụ thể, nếu gcd(m,n) = 1 thì tính ra chuỗi private_key và check như sau:

	i. d = inverse_mod(n,m)

	ii. pri_key [i] = pub_key[i]*d%m với i = 0 --> len(pub_key)

	iii. Kiểm tra xem pri_key có phải là chuỗi siêu tăng (superincreasing sequence) hay không. Nếu có thì trả về 	các giá trị pri_key,m,d.

{{Code: bruteforceKnap.py của mình mô tả việc brute force này}}

Mình tìm được các giá trị m = 4049, d = 3272 và prvkey = [8, 18, 29, 80, 164, 362, 1013, 2287]. Tuy nhiên, ciphertext của mã này là một dãy số, nên mình đoán ciphertext này là một dãy số được ghép lại. Tìm độ dài dãy, ta có len(c) = 124, lại có 4 kí tự đầu từ (1-4) và 4 kí tự ở vị trí từ (9-13) đều giống nhau. Vậy có thể 4 kí tự liền nhau là một số trong mảng ciphertext chăng? 

Thử giải tay 4 kí tự đầu là 0x11b9: 0x11b9 * 3272 % 4049 = 1430, tương ứng với 8 + 18 + 29 + 362 + 1013 = 11100110.

Vì biết định dạng flag là gigem{xxxxxxxxxxxx}, nên mình dễ dàng nhận ra dảo ngược chuỗi bits trên lại là 1100111 chính là chữ 'g'. Mình đã thử giải tiếp 4 kí tự kế xem phải là 'i' không:0x0d63 * 3272 % 4049 = 1463, tương ứng với 8 + 80 + 362 + 1013 = 10010110, viết ngược lại là 1101001, chính là kí tự 'i'.

Tiếp theo mình viết code để giải tất cả các chữ còn lại, và thu được flag.

{{Code: decipherKnap.py}}

FLAG: gig_em{merkle-hellman-knapsack}