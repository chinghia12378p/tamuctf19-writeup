Bài này để độ khó là easy, nhưng với mình thì cay cú nó vl :)). Với chuỗi đã cho, mình đã decode base64 và xem dưới dạng hexa để tìm điểm đặc biệt.
Đoạn mã sau khi decode base 64 và viết dưới dạng hexa: 5d405d4c5752730e570954464e09495c4a4c485a4e404e40555c49051a4b4f5d1a601a4857095b0956404e5d564c1a5a4e404e40555c490747.

Ngon, có 2 cái "5d" ở vị trí (1,2) và (5,6), giống với định dạng flag rồi. Giờ chỉ cần tìm quy luật nữa là xong. Nhưng sau ... mấy ngày cộng trừ tán loạn (mình nghĩ là sub cipher) mà mình vẫn không tìm ra quy luật gì. Rồi lại thêm cái tên đề bài ":)" làm mình cứ nghĩ đã đi sai hướng nữa, có lúc search google ":) cipher" luôn mới hài :)).
Sau khi hỏi anh mentor, mình quyết định làm tiếp theo hướng ban đầu. Đột nhiên một suy nghĩ lóe lên trong đầu mình là XOR cipher. Mình đã thử và BINGO! Ra quy luật rồi.

Quy luật thực ra khá đơn giản, kí tự ở vị trí chẵn (đếm từ 0) sẽ xor với 58 ra ciphertext và kí tự ở vị trí lẻ sẽ xor với 41 ra ciphertext. Còn mình thử như nào thì đơn giản lấy ciphertext hiện có xor với phần plaintext đã biết ("gigem{") thôi. Link: https://en.wikipedia.org/wiki/XOR_cipher.

Update: đây là mã hóa repeated XOR với Key XOR cố định và key có độ dài ngắn hơn plaintext. 58 và 41 trong mã ASCII lại chính là ":)" - tên của challenge ^^. 

Có quy luật rồi thì chỉ cần viết một đoạn code xor đơn giản là xong challenge.


FLAG: gigem{I'm not superstitious, but I am a little stitious.}


 