import base64
import hashlib
from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Util.number import *
class AESCipher(object):

    def __init__(self, key): 
        self.bs = 24
        self.key = key

    def encrypt(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc):
        print len(enc)
        enc = enc.decode('hex')
        print len(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('ascii','ignore')

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]

a = AESCipher("130222573707242246159397")
file = open("cipherCBC.txt",'r').read()
data = file.replace(" ","")
data = data.replace("\n","")
print a.decrypt(data)

