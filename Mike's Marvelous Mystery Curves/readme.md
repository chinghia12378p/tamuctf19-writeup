Đọc dữ liệu trong file key_exchange.pcap qua Follow Stream TCP, ta thu được nội dung của các file: 2 file certificate và 1 encrypt file bởi AES cipher. Theo mô tả của challenge, việc mã hóa có thể mô tả bằng sơ đồ sau:

		ECDH --->SharedKey--->AES-CBC cipher ---> ciphertext
                		         |		
	               		         |
	 		   Plaintext-----|

Về trao đổi khóa ECDH (Elliptic Curve Diffie Hellman), có thể mô tả ngắn gọn như sau:

1) Alice và Bob thống nhất sử dụng chung 1 đường cong E có Finite Field xác định và điểm cơ sở G.

2) Alice chọn 1 số bí mật lp, tính P = lp*G và gửi P cho Bob. Tương tự, Bob cũng chọn 1 số bí mật lq, tính Q = lq*G và gửi Q cho Alice.

3) Hai bên có chung khóa K = lp*lq*G, Alice tính bằng cách lấy lp*Q và Bob tính bằng cách lấy lq*P. Tuy nhiên, lưu ý rằng hacker dù có cả P và Q đều không thể tính được K.

Quay trở lại với challenge, tiến hành decode base64 2 file certificate thu được thông tin về ECDH như: điểm G, P, Q và đường cong E. Để tính SharedKey như sơ đồ mã hóa, ta phải tính được điểm K và khi đó Sharedkey = Kx | Ky, với Kx và Ky là tọa độ của điểm K.  
  
{{ alice_cert.txt và bob_cert.txt là 2 file certificate mà mình đã decode base64 }}

Tìm hiểu tiếp về các write-up challenge ctf nói về Eliptic curves trên GOOGLE, mình tìm được 1 write-up khá hay về cracking ECC: https://hgarrereyn.gitbooks.io/th3g3ntl3man-ctf-writeups/2017/picoCTF_2017/problems/cryptography/ECC2/ECC2.html. Write-up này mô tả tấn công phá mã Eliptic curve bằng thuật toán Pohlig-Hellman. Mình sẽ không đi quá sâu vào việc chứng minh tính đúng đắn hay cách cài đặt thuật toán mà chỉ lưu ý vài điểm về cuộc tấn công này (theo Attacking the Elliptic Curve Discrete Logarithm Problem của tác giả Matthew Musson):

1) Đầu tiên, thuật toán này chỉ hiệu quả nếu như order của Eliptic Curve có thể phân tích thành các thừa số nguyên tố  nhỏ. Order của Curve chính là tập hợp tất cả các điểm hữu hạn trong trường Finite Field p.

2) Thời gian để chạy thuật toán là phụ thuộc vào các giá trị Ki trong tập hợp các ước số nguyên tố của Order of Curve theo hàm căn bậc 2.  Nói cách khác, nếu Order of Curve có một thừa số nguyên tố quá lớn, thuật toán này sẽ không thể thực hiện được.

May mắn thay, Eliptic Curve trong challenge này có Finite Field nhỏ --> Order of Curves nhỏ --> Các thừa số nguyên tố của nó nhỏ. Vậy, chúng ta có thể áp dụng luôn thuật toán này để tìm các số bí mật lp, lq trong bài. Mình cũng sử dụng SageMath là một công cụ hỗ trợ tính toán rất mạnh với các lớp về đường cong đã xây dựng sẵn có hỗ trợ ngôn ngữ Python. 

Ngoài ra, ở mục References trong write-up trên, còn giới thiệu nhiều cách tấn công vào Eliptic Curve khác, các bạn có thể tham khảo.

Sau khi có được Sharekey, cũng chính là key của AES-CBC cipher, ta đã có thể giải mã được file cipherCBC.txt do AES là mã đối xứng. Để giải mã AES MODE CBC, ta cần thêm IV (Initialization vector) để cung cấp cho module Crypto.AES của Python. Theo như tài liệu ATTT của Đại học Nha Trang, GOOGLE, ... thì IV vector sẽ là 16 bytes đầu tiên trong ciphertext, nghĩa là:

	Ciphertext = IV (16 bytes) | Data_encrypted

Vậy ta đã có key + IV + Data_encrypted, tiếp theo chỉ cần cài đặt bộ giải mã AES từ module Crypto.AES của Python là xong.

{{cipherCBC.txt là nội dung hexa mình dump ra từ file pcap}}

FLAG: gigem{Forty-two_said_Deep_Thought}

