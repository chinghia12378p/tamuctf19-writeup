M = 412220184797
A = 10717230661382162362098424417014722231813
B = 22043581253918959176184702399480186312
P = (61801292647, 228288385004)
Q = (196393473219,35161195210 )
G = (56797798272,349018778637)
F = FiniteField(M)
E = EllipticCurve(F,[A,B])
P = E.point(P)
Q = E.point(Q)
G = E.point(G)
factors, exponents = zip(*factor(E.order()))
primes = [factors[i] ^ exponents[i] for i in range(len(factors))]
dlogs_p = []
dlogs_q = []
for fac in primes:
    t = int(P.order()) / int(fac)
    dlog_p = discrete_log(t*P,t*G,operation="+")
    dlogs_p += [dlog_p]
    dlog_q = discrete_log(t*Q,t*G,operation="+")
    dlogs_q += [dlog_q]
    #print("factor: "+str(fac)+", Discrete Log: "+str(dlog)) #calculates discrete logarithm for each prime order

lp = crt(dlogs_p,primes)
lq = crt(dlogs_q,primes)
print(lp*lq*G)