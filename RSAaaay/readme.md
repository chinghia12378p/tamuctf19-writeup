Bài này có tên RSAaaay nên chúng ta cũng đoán được ciphertext được mã hóa bằng RSA cipher.
 
RSA là hệ mã bất đối xứng gồm có 2 khóa: Public key được công khai và Private key được người dùng giữ kín. Public key chứa số mũ công khai (e) và modulus (n) trong khi Private key chứa số mũ bí mật (d) và modulo (n). Mã hóa RSA có các tính chất sau:

1) n = p*q với p,q là 2 số nguyên tố lớn (để tránh việc bị factor dễ dàng).

2) Số d và e có quan hệ: de = 1 (mod phi_n) với phi_n = (p-1)*(q-1). Trong đó, e là số được chọn, thường cũng là số nguyên tố.

3) Mã hóa: c = pow(m,e,n). Giải mã: m = pow(c,d,n). 
 
Dòng màu đỏ đầu tiên trong đề chính là Public key, trong đó ta xác định n = 2531257 và e = 43. Để phá mã hóa RSA, factor n nên là cách nên được thử đầu tiên. Ở đây ta nhận xét được n không quá lớn, nên việc factor n là khả thi. Sau khi có được p, q thì việc tính phi_n cũng như tìm d là rất dễ dàng (bằng thuật toán Euclid mở rộng). Mình factor n tại http://www.factordb.com/ và tính d bằng thuật toán Euclid mở rộng, hiện thực hóa bằng python như sau, thu được d = 58739.

	def egcd(a, b):
    		if a == 0:
        	   return (b, 0, 1)
    		g, y, x = egcd(b%a,a)
    		return (g, x - (b//a) * y, y)

	def modinv(a, m):
    		g, x, y = egcd(a, m)
    		if g != 1:
        		raise Exception('No modular inverse')
    		return x%m

Ciphertext của bài này là một mảng số. Mình cứ decrypt dần dần từng số xem thu được gì nào. Kết quả:

	Ciphertext		Plaintext
	906851			103
	991083			105103
	1780304			101109 
	2380434			12383 
	438490			97118 
	356019			97103 
	921472			10195 
	822283			83105 
	817856			12095 
	556932			70108 
	2102538			121105 
	2501908			110103 
	2211404			9584
	991083			105103 
	1562919			101114 
	38268			115125

Dễ dàng nhận ra là các kí tự ASCII có nghĩa từ các số trong dãy plaintext (ví dụ: 101109 = 101 (chữ 'e') và 109 (chữ 'm'), 105103 = 105 (chữ 'i') và 103 (chữ 'g'),...). Tới đây thì xong rồi, các bạn viết code để tách ra hay làm tay từ từ đều được.

{{ File solve.py của mình thực hiện việc tính các plaintext trên và tách các plaintext này thành các kí tự ASCII. }}

FLAG: gigem{Savage_Six_Flying_Tigers}